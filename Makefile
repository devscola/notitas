.PHONY: start

start:
	docker-compose up --build

build:
	docker-compose build

shell: build 
	docker-compose run --rm app bash

test: build
	docker-compose run --rm app npm run test

test-watch: build
	docker-compose run --rm app npm run test:watch

testApi-watch: build
	docker-compose run --rm api npm run test:watch

