# AppNotas 	:page_facing_up:

AppNotas is an app for taking quick notes to summarize a meeting, class, or ceremony.

## 1. Use 
It can be used online by accessing: **https://notitas.vercel.app/**
We can add notes by simply pressing Enter or by pressing the Add button.

##### To remove the notes: :put_litter_in_its_place:
   * Via the console by typing: localStorage.clear()
   * In the Application/Local Storage/notitas.vercel.app tab by pressing Clear 

## 2. Requirements
* Docker

##3. How to Install and Run the Project 🔧
##### Can be installed using git:
   `$ git clone https://gitlab.com/devscola/notitas.git`

### Commands:
### Starts the container

`$ make start`

Or its version without use makefile:

`$ docker run --rm -it -v $$(pwd)/src:/voicedown/src -p 4200:4200 voicedown`

Now you can use the app in: **http://localhost:4200/**

#### Starts the container and access it with the console.

`$ make shell`

Or its version without use makefile:

`$ docker run --rm -it -v $$(pwd)/src:/voicedown/src voicedown bash`

#### Launch tests with Jest

`$ make test`

Or its version without use makefile:

`$ docker run --rm -it -v $$(pwd)/src:/voicedown/src voicedown npm run test`

#### Launch tests with Jest and listening for changes and launching again if any files changes.

`$ make test-watch`

Or its version without use makefile:

`$ docker run --rm -it -v $$(pwd)/src:/voicedown/src voicedown npm run test:watch`

---

It builds Docker images from a Dockerfile searching a Dockerfile in the path ".", and tagging the image with the name voicedown

`docker build -t voicedown .`

It creates a writeable container layer over the specified image (in this case a image called voicedown), and then starts it using a specified command. 

Starting it with `--rm` will the container to be removed when it is closed.

By using `-it` will be interactive so we can use the console.

The `-v` command allows us to mount a volume to share files between the dockerized image and the localhost in case we want to modify the code.

The `-p` command creates a gateway between the container ports and the localhost, so we can access the app by using our navigator.

##4. Version


