import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { routes } from '../app-routing.module';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-notebook',
  templateUrl: './notebook.component.html',
  styleUrls: ['./notebook.component.scss'],
})
export class NotebookComponent implements OnInit {
  @ViewChild('scrollToButtom', { static: true })
  private containerToScroll!: ElementRef;
  notes: string[] = [];
  rutas = routes;
  collection = 'notitas';

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.scrollToBottom();

    this.route.paramMap.subscribe((params) => {
      if (params.get('id')) {
        this.collection = params.get('id')!;
      }
    });

    let notesStorage: string | null = localStorage.getItem(this.collection);
    if (notesStorage) {
      this.notes = JSON.parse(notesStorage);
    }
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  storeNote(note: string) {
    this.notes.push(note);
    localStorage.setItem(this.collection, JSON.stringify(this.notes));
  }

  scrollToBottom(): void {
    try {
      this.containerToScroll.nativeElement.scrollTop =
        this.containerToScroll.nativeElement.scrollHeight;
    } catch (err) {
      console.log('ERROR:', err);
    }
  }
}
