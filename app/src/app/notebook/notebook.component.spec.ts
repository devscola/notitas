import { screen, fireEvent, render } from '@testing-library/angular';
import { AddNoteComponent } from '../add-note/add-note.component';
import { NotebookComponent } from './notebook.component';
import { FormsModule } from '@angular/forms';
import userEvent from '@testing-library/user-event';
import { AppComponent } from '../app.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('NotebookComponent', () => {
  const testNoteOne = 'Hola Mundo';
  const testNoteTwo = 'Adios Mundo';

  beforeEach(() => {
    localStorage.clear();
  });

  it('shows the note that has been created', async () => {
    await renderComponent();

    await writeText(testNoteOne);
    clickAddButton();

    expect(screen.queryByText(testNoteOne)).toBeInTheDocument();
  });

  it('does not shows text note while typing', async () => {
    await renderComponent();

    await writeText(testNoteOne);

    expect(screen.queryByText(testNoteOne)).not.toBeInTheDocument();
  });

  it('show more than one note', async () => {
    await renderComponent();

    await addNote(testNoteOne);
    await addNote(testNoteTwo);

    expect(screen.queryByText(testNoteOne)).toBeInTheDocument();
    expect(screen.queryByText(testNoteTwo)).toBeInTheDocument();
  });

  it('shows the notes after refreshing the page', async () => {
    localStorage.setItem('notas', JSON.stringify([testNoteOne]));
    const renderComponent = await renderComponentURL();

    await renderComponent.navigate('notas');

    expect(screen.queryByText(testNoteOne)).toBeInTheDocument();
  });

  it('Shows a URL like a title', async () => {
    const renderComponent = await renderComponentURL();

    await renderComponent.navigate('notas');

    expect(await screen.findByText('notas')).toBeInTheDocument();
  });

  it('Shows a new collection based on title', async () => {
    localStorage.setItem('notas', JSON.stringify([testNoteOne]));
    const renderComponent = await renderComponentURL();

    await renderComponent.navigate('notas');

    expect(screen.queryByText(testNoteOne)).toBeInTheDocument();
  });

  const writeText = async (message: string) => {
    await userEvent.type(
      screen.getByRole('textbox', { hidden: true }),
      message
    );
  };

  const addNote = async (message: string) => {
    await writeText(message);
    clickAddButton();
  };

  const clickAddButton = () => {
    fireEvent.click(screen.getByText('Add'));
  };

  const renderComponent = async () => {
    return await render(NotebookComponent, {
      declarations: [AddNoteComponent],
      imports: [FormsModule],
    });
  };

  const renderComponentURL = async () => {
    return await render(AppComponent, {
      declarations: [NotebookComponent],
      routes: [
        {
          path: '',
          children: [
            {
              path: ':id',
              component: NotebookComponent,
            },
          ],
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
  };
});
