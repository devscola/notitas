import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AddNoteComponent } from './add-note/add-note.component';
import { NotebookComponent } from './notebook/notebook.component';
import { NotesService } from './notes.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent, AddNoteComponent, NotebookComponent],
  imports: [
    FormsModule, 
    ReactiveFormsModule, 
    BrowserModule, 
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [NotesService],
  bootstrap: [AppComponent],
})
export class AppModule {}
