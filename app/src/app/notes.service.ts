import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  constructor(private http: HttpClient) {

   }

   getNotes(): Observable<any> {
    return this.http.get("http://localhost:3000/notitas")
   }
}
