import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotebookComponent } from './notebook/notebook.component';

export const routes: Routes = [
  { path: '', component: NotebookComponent },
  { path: ':id', component: NotebookComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
