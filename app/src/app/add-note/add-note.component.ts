import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss'],
})
export class AddNoteComponent implements OnInit {
  textButton: string = 'Add';
  draft: string = '';
  @Output() sendNote = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  sendDraft() {
    const fixedNote = this.draft.trimEnd();
    if (fixedNote) {
      this.sendNote.emit(fixedNote);
    }
    this.clearTextArea();
  }

  onKeydown(event: Event) {
    event.preventDefault();
  }

  clearTextArea() {
    this.draft = '';
  }
}
