import { fireEvent, screen, render, waitFor } from '@testing-library/angular';
import { AddNoteComponent } from './add-note.component';
import { FormsModule } from '@angular/forms';
import userEvent from '@testing-library/user-event';

describe('AddNoteComponent', () => {
  const testText = 'Test text.';
  const placeholderText = 'Type here and press enter to send note';

  it('emits input text when button is clicked', async () => {
    const callback = await spyRenderComponent();

    await addNoteWithButton(testText);

    expect(callback).toHaveBeenCalledWith(testText);
  });

  it('Clears text area when button is clicked', async () => {
    await renderComponent();

    await addNoteWithButton(testText);

    await waitFor(() =>
      expect(screen.getByRole('textbox', { hidden: true })).toHaveValue('')
    );
  });

  it('gets the placeholder by default', async () => {
    await renderComponent();

    const placeholderElement = screen.queryByPlaceholderText(placeholderText);

    await waitFor(() => expect(placeholderElement).toBeInTheDocument());
  });

  it('is writable', async () => {
    await renderComponent();

    await writeDraft(testText);

    expect(screen.getByRole('textbox', { hidden: true })).toHaveValue(testText);
  });

  it('emits input text when enter key is pressed', async () => {
    const callback = await spyRenderComponent();

    await writeDraft(testText + '{enter}');

    expect(callback).toHaveBeenCalledWith(testText);
  });

  it('not sending input text when it is empty', async () => {
    const callback = await spyRenderComponent();

    await addNoteWithButton('{enter}');

    expect(callback).toHaveBeenCalledTimes(0);
  });

  const renderComponent = async () => {
    await render(AddNoteComponent, {
      imports: [FormsModule],
    });
  };

  const spyRenderComponent = async () => {
    const callback = jest.fn();
    await render(`<app-add-note (sendNote)="spy($event)">`, {
      declarations: [AddNoteComponent],
      imports: [FormsModule],
      componentProperties: {
        spy: callback,
      },
    });
    return callback;
  };

  const writeDraft = async (text: string) => {
    await userEvent.type(screen.getByRole('textbox', { hidden: true }), text);
  };

  const sendButton = () => {
    fireEvent.click(screen.getByText('Add'));
  };

  const addNoteWithButton = async (text: string) => {
    await writeDraft(text);
    sendButton();
  };
});
