import app from "./app"

const port = 3000

app.listen(port, () => {
    console.log(`Timezones by location application is running on port ${port}.`)
  })