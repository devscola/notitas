import app from "./app"
import request from "supertest"

describe("Api", () => {
  describe("GET /notitas",() => {
    it("returns notes", async () => {
      const note = ["Test note"]
  
      const response = await request(app).get("/notitas")
  
      expect(response.body).toEqual(note)
      expect(response.statusCode).toEqual(200)
    })
  })
})
