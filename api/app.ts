import express, { Application, Router } from "express"
import cors from "cors"

const app = express()

app.use(cors())

app.get("/notitas", (req, res) => {
  const notes = ["Test note"]
  res.send(notes)
})

export default app
